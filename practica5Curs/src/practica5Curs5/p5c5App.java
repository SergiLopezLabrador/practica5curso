package practica5Curs5;

import javax.swing.JOptionPane;

public class p5c5App {

	public static void main(String[] args) {
		
		//Aqu� pido un n�mero en formato String a trav�s de la funci�n JOptionPane y posteriormente lo convertir� en una variable Double
		String NUMERO = JOptionPane.showInputDialog(null, "Introduce el n�mero:");
		double numeroDouble = Double.parseDouble(NUMERO);
		//Aqu� hago dos condiciones
		//En la primera, si el numero que ha escrito el usuario �s divisible de 2, saldr� un mensaje por pantalla indicando que es divisible
		if(numeroDouble%2==0) {
			JOptionPane.showMessageDialog(null, "El numero " + numeroDouble + " �s divisible entre 2");
		}
		//Y en caso contrario, saldr� un mensaje por pantalla indicando que no es divisible
		else if(!(numeroDouble%2==0)) {
			JOptionPane.showMessageDialog(null, "El numero " + numeroDouble + " NO �s divisible entre 2");
		}

	}

}
