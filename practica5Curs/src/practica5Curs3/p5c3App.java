package practica5Curs3;

import javax.swing.JOptionPane;

public class p5c3App {

	public static void main(String[] args) {
		//Aqu�, a trav�s de la herramienta JOptionPane, pido el nombre al usuario para posteriormente mostrarlo por pantalla
		String nombre = JOptionPane.showInputDialog(null, "Introduce su nombre:");
		//Aqu�, vuelvo a utilizar la misma herramienta, pero esta vez para mostrar el mensaje por pantalla con el nombre que 
		//ha seleccionado el usuario
		JOptionPane.showMessageDialog(null, "Bienvenido, tu nombre �s " + nombre);

	}

}
