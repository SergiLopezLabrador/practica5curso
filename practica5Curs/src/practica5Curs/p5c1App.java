package practica5Curs;

public class p5c1App {

	public static void main(String[] args) {
		//Aqu� creo dos variables Integer, una de ellas con el valor de 5 y la otra con el valor de 10
		int A = 5;
		int B = 10;
		//Aqu� asignar� 3 condiciones con la herramienta if
		//En la primera condici�n se mostrar� el mensaje del System.out.println en el caso de que se cumpla la condici�n (A>B)
		if (A>B) {
			System.out.println("La variable A con valor: " + A + " �s mayor a la variable B con valor de: " + B);
		}
		//Si la primera condici�n no se cumple, pasaremos a esta segunda, donde la condici�n es diferente
		else if (A<B) {
			System.out.println("La variable B con valor: " + B + " �s mayor a la variable A con valor de: " + A);
		}
		//Finalmente, si la anterior condici�n no se cumple, pasaremos a la ultima condici�n
		else if (A == B) {
			System.out.println("La variable A con valor: " + A + " �s igual a la variable B con valor de: " + B);
		}

	}

}
