package practica5Curs9;

public class p5c9App {

	public static void main(String[] args) {
		//Aqu� creo un bucle for donde mostrar� un contador que incrementar� de 1 al 100, con el 100 incluido
		for (int numero = 0; numero <= 100; numero++) {
			System.out.println(numero);
			//En el caso de que un n�mero sea divisible de 2, se mostrar� por la consola
			if(numero%2==0) {
				System.out.println("El n�mero: " + numero + " �s divisible entre 2");
			}
			//En el caso de que un n�mero sea divisible de 3, se mostrar� por la consola
			else if(numero%3==0) {
				System.out.println("El n�mero: " + numero + " �s divisible entre 3");
			}
		}

	}

}
