package practica5Curs7;

public class p5c7App {

	public static void main(String[] args) {
		
		int numero = 0;
		//En este ejercicio, he creado un bucle tipo While que en el caso de que el n�mero sea inferior o igual a 100
		// se incrementar� en 1 el mismo contador hasta llegar al m�ximo establecido.
		while(numero<=100) {
			System.out.println(numero++);
		}

	}

}
