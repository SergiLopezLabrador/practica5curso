package practica5Curs10;

import javax.swing.JOptionPane;

public class p5c10App {

	public static void main(String[] args) {
		//Aqu� pido al usuario que escriba un n�mero de ventas. Utilizaremos la funci�n JOptionPane para que salga en formato mensaje.
		String ventas = JOptionPane.showInputDialog(null, "Introduce el n�mero de ventas:");
		int ventasInt = Integer.parseInt(ventas);
		double suma = 0;
		//Aqu� he creado un bucle de tipo For donde por cada venta, preguntar� el precio de la misma y se i� sumando consecutivamente
		for (int i = 0; i < ventasInt; i++) {
			String precioVenta = JOptionPane.showInputDialog(null, "Introduce el precio de la venta:");
			double precioVentaDouble = Double.parseDouble(precioVenta);

			suma = suma + precioVentaDouble;
		}
		//Finalmente, muestro la suma de todos los productos
		System.out.println("Suma de todos los precios de ventas: " + suma);

	}

}
