package practica5Curs4;

import javax.swing.JOptionPane;

public class p5c4App {

	public static void main(String[] args) {
		//Aqu� pido el radio en formato String a trav�s de la funci�n JOptionPane y posteriormente lo convertir� en una variable Double
		String radio = JOptionPane.showInputDialog(null, "Introduce el radio:");
		double radioDouble = Double.parseDouble(radio);
		//Finalmente, hago que surga una ventana donde muestre la operaci�n y su resultado
		JOptionPane.showMessageDialog(null, "La �rea del circulo �s: " + (Math.PI*Math.pow(radioDouble,2))  );

	}

}
