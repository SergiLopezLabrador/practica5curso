package practica5Curs11;

import javax.swing.JOptionPane;

public class p5c11App {

	public static void main(String[] args) {
		//Aqu� pido al usuario que escriba un d�a de la semana. Utilizaremos la funci�n JOptionPane para que salga en formato mensaje.
		String DIASEMANA = JOptionPane.showInputDialog(null, "Introduce el d�a de la semana:");
		//He creado un switch donde hago un case por cada d�a de la semana indicando si el d�a es laboral o no laboral.
		switch (DIASEMANA) {
		case "Lunes":
			JOptionPane.showMessageDialog(null, "Lunes �s un d�a laboral");
			break;
		case "Martes":
			JOptionPane.showMessageDialog(null, "Martes �s un d�a laboral");
			break;
		case "Miercoles":
			JOptionPane.showMessageDialog(null, "Miercoles �s un d�a laboral");
			break;
		case "Jueves":
			JOptionPane.showMessageDialog(null, "Jueves �s un d�a laboral");
			break;
		case "Viernes":
			JOptionPane.showMessageDialog(null, "Viernes �s un d�a laboral");
			break;
		case "S�bado":
			JOptionPane.showMessageDialog(null, "S�bado NO �s un d�a laboral");
			break;
		case "Domingo":
			JOptionPane.showMessageDialog(null, "Domingo NO �s un d�a laboral");
			break;
		//En el caso de que no se introduzca un d�a de la semana v�lido, el mensaje del apartado Default se mostrar� en pantalla
		// y el programa finalizar�
		default:
			JOptionPane.showMessageDialog(null, "Introduce un d�a de la semana v�lido");
			break;
		}

	}

}
