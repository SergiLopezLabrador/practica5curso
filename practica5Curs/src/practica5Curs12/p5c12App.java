package practica5Curs12;

import javax.swing.JOptionPane;

public class p5c12App {

	public static void main(String[] args) {
		
		//Aqu� he pedido la contrase�a al usuario a trav�s de la funci�n JOptionPane
		String CONTRASENA = JOptionPane.showInputDialog(null, "Introduce tu contrase�a:");
		String ComprobarContrasena = " ";
		int contador = 0;

		//Aqu� he realizado un bucle do/while para comprobar que hasta que no se cumpla la condici�n de While() no se pueda salir del mismo
		do {
	
			contador++;
			ComprobarContrasena = JOptionPane.showInputDialog(null, "Cual �s la contrase�a?: " + contador + "/3");
		}

		while(!CONTRASENA.equals(ComprobarContrasena) &&  contador <= 3 );
		//En el caso de que se descubra la contrase�a antes de superar 3 intentos, te aparecer� el mensaje de Enhorabuena
		if(CONTRASENA.equals(ComprobarContrasena) &&  contador <= 3) {
	JOptionPane.showMessageDialog(null, "Enhorabuena");
}

 }

	}
