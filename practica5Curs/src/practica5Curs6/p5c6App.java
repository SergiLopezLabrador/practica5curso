package practica5Curs6;

import javax.swing.JOptionPane;

public class p5c6App {

	public static void main(String[] args) {
		//Aqu� pido por mensaje al usuario que me introduzca el precio de un producto a trav�s de la funci�n JOptionPane
		String NUMERO = JOptionPane.showInputDialog(null, "Introduce el n�mero:");
		double numeroDouble = Double.parseDouble(NUMERO);
		//Aqu� creo una variable para remarcar el IVA que usaremos posteriormente
		double IVA = numeroDouble/100*21;
		//Finalmente, muestro una serie de mensajes por la consola ense�ado el precio del producto, su IVA y su precio final
		//con el IVA sumado
		System.out.println("Precio del producto: " + numeroDouble);
		System.out.println("Precio de IVA (21% del precio del producto): " + IVA);
		System.out.println("Precio final con IVA: " + (numeroDouble+IVA));
		

	}

}
