package practica5Curs13;

import javax.swing.JOptionPane;

public class p5c13App {

	public static void main(String[] args) {
		
		//Aqu� inicializo las variables, tanto el primer n�mero como el segundo.
		//Tambi�n he creado la variable String llamada Signo para que el usuario pueda introducir un signo v�lido
		String numero1 = JOptionPane.showInputDialog(null, "Introduce un n�mero:");
		double numero1Double = Double.parseDouble(numero1);
		
		String numero2 = JOptionPane.showInputDialog(null, "Introduce un n�mero:");
		double numero2Double = Double.parseDouble(numero2);
		
		String SIGNO = JOptionPane.showInputDialog(null, "Introduce un signo:");
		
		
		//A partir de aqu�, hago un switch/case donde establezco una operaci�n dependiendo del signo que ha elegido el usuario 
		switch (SIGNO) {
		case "+":
			JOptionPane.showMessageDialog(null, "Suma entre: " + numero1 + " y " + numero2 + " �s: " + (numero1Double+numero2Double));
			break;
		case "-":
			JOptionPane.showMessageDialog(null, "Resta entre: " + numero1 + " y " + numero2 + " �s: " + (numero1Double-numero2Double));
			break;
		case "*":
			JOptionPane.showMessageDialog(null, "Multiplicaci�n entre: " + numero1 + " y " + numero2 + " �s: " + (numero1Double*numero2Double));
			break;
		case "/":
			JOptionPane.showMessageDialog(null, "Dividir entre: " + numero1 + " y " + numero2 + " �s: " + (numero1Double/numero2Double));
			break;
		case "^":
			JOptionPane.showMessageDialog(null, "Cuadrado entre: " + numero1 + " y " + numero2 + " �s: " + (Math.pow(numero1Double, numero2Double)));
			break;
		case "%":
			JOptionPane.showMessageDialog(null, "M�dulo entre: " + numero1 + " y " + numero2 + " �s: " + (numero1Double%numero2Double));
			break;
		//En el caso de que no haya escogido un signo v�lido, le saldr� el mensaje de la parte inferior
		default:
			JOptionPane.showMessageDialog(null, "Elige un s�mbolo v�lido");
			break;
		}

	}

}
